import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule,
    MatProgressSpinnerModule, MatPaginatorModule, MatSortModule, MatInputModule, MatDialogModule, MatSelectModule, MatCheckboxModule } from '@angular/material';

@NgModule({
    imports: [LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule,
        MatProgressSpinnerModule, MatPaginatorModule, MatSortModule, MatInputModule, MatDialogModule, MatSelectModule, MatCheckboxModule],
    exports: [LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule,
        MatProgressSpinnerModule, MatPaginatorModule, MatSortModule, MatInputModule, MatDialogModule, MatSelectModule, MatCheckboxModule],
})

export class MaterialModule {}