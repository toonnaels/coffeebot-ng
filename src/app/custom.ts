import { NgModule } from '@angular/core';
import { ConcatPipe } from './custom/pipes/concat.pipe';

@NgModule({
    imports: [],
    declarations: [
        ConcatPipe
    ],
    exports: [
        ConcatPipe
    ],
})

export class CustomModule {}