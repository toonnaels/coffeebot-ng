import { User, LoadCredit } from "./user.model";

export class UserRequest {
  name: string;
  surname: string;
  studentNumber: string;
  cardUUID: string;
  balance: number;

  constructor(user: User) {
    this.name = user.name;
    this.surname = user.surname;
    this.studentNumber = user.studentNumber;
    this.cardUUID = user.cardUUID;
    this.balance = user.balance;
  }
}

export class LoadCreditRequest {
  id: string;
  amount: number;

  constructor(loadCredit: LoadCredit) {
    this.id = loadCredit.id;
    this.amount = loadCredit.amount;
  }
}