import { UserResponse, TopDrinkersResponse, TopDrinkerUserResponse, TopDebtorsResponse } from "./user-response";

export class User {
    id?: string;
    name: string;
    surname: string;
    studentNumber: string;
    cardUUID: string;
    balance: number;

    constructor(userResponse?: UserResponse) {
        if(userResponse) {
            this.id = userResponse.id;
            this.name = userResponse.name;
            this.surname = userResponse.surname;
            this.studentNumber = userResponse.studentNumber;
            this.cardUUID = userResponse.cardUUID;
            this.balance = userResponse.balance;
        }
    }
}

export class TopDrinker {
    purchases: number;
    user: User;

    constructor(topDrinkerResponse: TopDrinkerUserResponse) {
        this.purchases = topDrinkerResponse.purchases;
        this.user = new User(topDrinkerResponse.user);
    }
}
  
export class TopDrinkers {
    day: TopDrinker[];
    week: TopDrinker[];
    month: TopDrinker[];
    year: TopDrinker[];

    constructor(topDrinkersResponse: TopDrinkersResponse) {
        this.day = topDrinkersResponse.day.map((topDrinker) => new TopDrinker(topDrinker));
        this.week = topDrinkersResponse.week.map((topDrinker) => new TopDrinker(topDrinker));
        this.month = topDrinkersResponse.month.map((topDrinker) => new TopDrinker(topDrinker));
        this.year = topDrinkersResponse.year.map((topDrinker) => new TopDrinker(topDrinker));
    }
}

export class TopDebtors {
    users: User[];

    constructor(topDebtorsResponse: TopDebtorsResponse) {
        this.users = topDebtorsResponse.users.map(userResponse => new User(userResponse));
    }
}

export class LoadCredit {
    id: string;
    amount: number;

    constructor(id: string, amount: number) {
        this.id = id;
        this.amount = amount;
    } 
}