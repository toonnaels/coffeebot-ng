export class UserResponse {
  id: string;
  name: string;
  surname: string;
  studentNumber: string;
  cardUUID: string;
  balance: number;
}

export class TopDrinkerUserResponse {
  purchases: number;
  user: UserResponse;
}

export class TopDrinkersResponse {
  day: TopDrinkerUserResponse[];
  week: TopDrinkerUserResponse[];
  month: TopDrinkerUserResponse[];
  year: TopDrinkerUserResponse[];
}

export class TopDebtorsResponse {
  users: UserResponse[];
}