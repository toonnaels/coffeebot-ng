import { UnregisteredUserResponse } from "./unregistered-user-response";

export class UnregisteredUser {
    id: string;
    cardUUID: string;

    constructor(response: UnregisteredUserResponse) {
        this.id = response.id;
        this.cardUUID = response.cardUUID;
    }
}