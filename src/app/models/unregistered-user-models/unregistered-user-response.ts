export class UnregisteredUserResponse {
    id: string;
    cardUUID: string;
}