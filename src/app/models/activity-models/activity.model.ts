import { ActivityResponse } from "./activity-response";

export class Activity {
    id?: string;
    date: Date;
    description: string;

    constructor(activityResponse?: ActivityResponse) {
        if(activityResponse) {
            this.id = activityResponse.id;
            this.date = activityResponse.date;
            this.description = activityResponse.description;
        }
    }
}