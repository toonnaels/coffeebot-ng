export class ActivityResponse {
  id: string;
  date: Date;
  description: string;
}