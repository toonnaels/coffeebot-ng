import { Activity } from "./activity.model";

export class ActivityRequest {
  date: Date;
  description: string;

  constructor(activity: Activity) {
    this.date = activity.date;
    this.description = activity.description;
  }
}