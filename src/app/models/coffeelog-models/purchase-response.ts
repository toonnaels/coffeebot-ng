import { User } from '../user-models/user.model';
import { CoffeeOption } from './coffee-option.model';

export class PurchaseResponse {
    id: string;
    date: Date;
    userId: string;
    user: User;
    itemId: string;
    item: CoffeeOption;
}