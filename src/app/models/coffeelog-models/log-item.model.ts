import { PurchaseResponse } from './purchase-response';

export enum ItemType {
    coffee = 'Coffee Only',
    milkcoffee = 'Coffee & Milk',
    milk = 'Milk'
}

export class LogItem {
    name: string;
    surname: string;
    item: ItemType;
    price: number;
    date: Date;

    constructor(purchaseResponse: PurchaseResponse) {
        this.name = purchaseResponse.user.name;
        this.surname = purchaseResponse.user.surname;
        this.item = ItemType[purchaseResponse.item.item];
        this.price = purchaseResponse.item.price;
        this.date = purchaseResponse.date;
    }
}