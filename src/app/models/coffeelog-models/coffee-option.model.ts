export class CoffeeOption {
    id: string;
    item: string;
    price: number;
}