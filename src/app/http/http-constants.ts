import { environment } from '../../environments/environment';

export class HttpConstants {
    // URLs.
    public static baseUrl = (environment.production) ? "" : "http://localhost:3000";
    public static serverUrl = `${HttpConstants.baseUrl}/api/`;

    // Config.
    public static loggedInToken = 'coffeebot-admin-token';
    public static adminName = 'coffeebot-admin-name';
    public static adminSurname = 'coffeebot-admin-surname';
    public static adminEmail = 'coffeebot-admin-email';
    public static adminId = 'coffeebot-admin-id'
}