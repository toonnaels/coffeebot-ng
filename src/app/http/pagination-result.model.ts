export class PaginatedResponse<A> {
    constructor(public page: number, public totalPages: number, public totalRecords: number, public response: A[]) {
    }
}