import { Observable, throwError } from "rxjs";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { retry, catchError, delay } from "rxjs/operators";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

export function httpGet<A>(http: HttpClient, url: string, options: object, errorMessage: string): Observable<A> {
    return http.get<A>(url, options).pipe(
        delay(0),
        // retry(5),
        catchError((error) => handleError(error, errorMessage))
    );
}

export function httpPost<A, B>(http: HttpClient, url: string, body: A, errorMessage: string): Observable<B> {
    return http.post<B>(url, body, httpOptions).pipe(
        delay(0),
        // retry(5),
        catchError((error) => handleError(error, errorMessage))
    );
}

export function httpDelete(http: HttpClient, url: string, errorMessage: string): Observable<{}> {
    return http.delete(url, httpOptions).pipe(
        delay(0),
        // retry(5),
        catchError((error) => handleError(error, errorMessage))
    );
}

export function httpPut<A>(http: HttpClient, url: string, body: A, errorMessage: string): Observable<A> {
    return http.put<A>(url, body, httpOptions).pipe(
        delay(0),
        // retry(5),
        catchError((error) => handleError(error, errorMessage))
    );
}

function handleError(error: HttpErrorResponse, errorMessage: string) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
    }
    // return an observable with a user-facing error message
    return throwError(errorMessage);
  };