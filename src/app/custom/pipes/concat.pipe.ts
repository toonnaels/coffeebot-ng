import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'concat'})
export class ConcatPipe implements PipeTransform {
    transform(prefix: string, suffix: string): string {
        return `${prefix} ${suffix}`;
    }
}