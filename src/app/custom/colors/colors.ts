export class Colors {
    public static BLUE_DARK: string = '#2196F3'; /* mat-blue, 500 */
    public static BLUE_LIGHT: string = '#90CAF9'; /* mat-blue, 200 */

    public static GRAY_DARK: string = '#9E9E9E'; /* mat-gray, 500 */
    public static GRAY_LIGHT: string = '#EEEEEE'; /* mat-gray, 200 */

    public static  YELLOW_DARK: string = '#FFC107'; /* mat-amber, 500 */
    public static YELLOW_LIGHT: string = '#FFE082'; /* mat-amber, 200 */
    
    public static RED_DARK: string = '#F44336'; /* mat-red, 500 */
    public static RED_LIGHT: string = '#EF9A9A'; /* mat-red, 200 */

    public static GREEN_DARK: string = '#4CAF50'; /* mat-green, 500 */
    public static GREEN_LIGHT: string = '#A5D6A7'; /* mat-green, 200 */
    
    public static BROWN_DARK: string = '#795548'; /* mat-brown, 500 */
    public static BROWN_LIGHT: string = '#BCAAA4'; /* mat-brown, 200 */
    
    public static DEEP_RED: string = '#B71C1C'; /* mat-red, 900 */
    public static DEEP_GREEN: string = '#1B5E20'; /* mat-green, 900 */

    public static SILVER_DARK: string = '#9E9E9E'; /* mat-gray, 500 */
    public static SILVER_LIGHT: string = '#E0E0E0'; /* mat-gray, 300 */
}