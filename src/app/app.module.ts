import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { HttpClientModule } from '@angular/common/http';

import { CoffeebotModule } from './coffeebot/coffeebot.module';
import { AdminModule } from './admin/admin.module';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { NewUserDialogComponent } from './admin/user-management/new-user-dialog/new-user-dialog.component';
import { LoadCreditDialogComponent } from './admin/user-management/load-credit-dialog/load-credit-dialog.component';
import { ChangePasswordComponent } from './admin/admin-page/change-password/change-password.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    CoffeebotModule,
    AdminModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [],
  entryComponents: [
    ErrorDialogComponent,
    NewUserDialogComponent,
    LoadCreditDialogComponent,
    ChangePasswordComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
 