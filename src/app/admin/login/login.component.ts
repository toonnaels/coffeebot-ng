import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AdminService } from '../service/admin.service';
import { LoginCredentials } from '../models/login-credentials';
import { MatDialog } from '@angular/material';
import { ErrorDialogComponent } from '../../error-dialog/error-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private adminPageUrl = '/admin/management';

  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);

  constructor(private adminService: AdminService, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
  }

  login() {
    if(!this.email.errors && !this.password.errors) {
      const credentials = new LoginCredentials(this.email.value, this.password.value);
      this.adminService.login(credentials).subscribe(
        login => {
          if(login.authenticated) {
            this.router.navigate([this.adminPageUrl]);
          } else {
            this.dialog.open(ErrorDialogComponent, {
              width: '20em',
              data: { errorMessage: login.message }
            });
          }
        },
        error => {
          this.dialog.open(ErrorDialogComponent, {
            width: '20em',
            data: { errorMessage: error }
          });
        }
      );
    }
  }

  getErrorMessage(field: string) {
    if(field === 'email') {
      return this.email.hasError('required') ? 'You must enter a value' : this.email.hasError('email') ? 'Not a valid email' : '';
    } else if(field === 'password') {
      return this.password.hasError('required') ? 'You must enter a password' : '';
    }
  }

}
