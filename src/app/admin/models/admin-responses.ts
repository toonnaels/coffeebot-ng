export class AdminUserResponse {
    id: string;
    name: string;
    surname: string;
    email: string;
}

export class LoginResponse {
    authenticated: boolean;
    message: string;
    token: string;
    admin: AdminUserResponse;
}

export class ChangePasswordResponse {
    updated: boolean;
}