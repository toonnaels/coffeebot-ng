import { CoffeeOptions, CoffeeOption } from "./coffeeprice-request";

export class CoffeePrice {
    coffee_id: string;
    milk_id: string;
    milk_coffee_id: string;
    coffee: number;
    milk: number;

    constructor(coffeeoptions?: CoffeeOptions) {
        if(coffeeoptions) {
            var i;
            for(i = 0 ; i < coffeeoptions.options.length ; i++) {
                var option: CoffeeOption = coffeeoptions.options[i];
                switch(option.item) {
                    case "coffee": {
                        this.coffee_id = option.id;
                        this.coffee = option.price;
                        break;
                    }
                    case "milk": {
                        this.milk_id = option.id;
                        this.milk = option.price;
                        break;
                    }
                    case "milkcoffee": {
                        this.milk_coffee_id = option.id;
                        break;
                    }
                }
            }
        }
    }
}