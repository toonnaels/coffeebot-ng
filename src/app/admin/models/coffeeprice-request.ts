import { CoffeePrice } from "./coffeeprice.model";

export class CoffeeOptions {
    options: CoffeeOption[];

    constructor(coffeeprice?: CoffeePrice) {
        if(coffeeprice) {
            const coffee: CoffeeOption = new CoffeeOption(coffeeprice.coffee_id, "coffee", coffeeprice.coffee);
            const milk: CoffeeOption = new CoffeeOption(coffeeprice.milk_id, "milk", coffeeprice.milk);
            const milk_coffee: CoffeeOption = new CoffeeOption(coffeeprice.milk_coffee_id, "milkcoffee", coffeeprice.coffee + coffeeprice.milk);
    
            this.options = [coffee, milk, milk_coffee];
        }
    }
}

export class CoffeeOption {
    id: string;
    item: string;
    price: number;

    constructor(id: string, item: string, price: number) {
        this.id = id;
        this.item = item;
        this.price = price;
    }
}