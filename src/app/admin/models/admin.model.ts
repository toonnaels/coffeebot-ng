import { AdminUserResponse, LoginResponse } from './admin-responses';

export class AdminUser {
    id: string;
    name: string;
    surname: string;
    email: string;

    constructor(adminUserResponse: AdminUserResponse = new AdminUserResponse()) {
        this.id = adminUserResponse.id;
        this.name = adminUserResponse.name;
        this.surname = adminUserResponse.surname;
        this.email = adminUserResponse.email;
    }
}

export class Login {
    authenticated: boolean;
    message: string;
    admin: AdminUser;

    constructor(loginResponse: LoginResponse) {
        this.authenticated = loginResponse.authenticated;
        this.message = loginResponse.message;
        this.admin = new AdminUser(loginResponse.admin);
    }
}