import { AdminUser } from "./admin.model";

export class LoginRequest {
    email: string;
    password: string;

    constructor(email: string, password: string) {
        this.email = email;
        this.password = password;
    }
}

export class AdminUserRequest {
    id: string;
    name: string;
    surname: string;
    email: string;

    constructor(adminUser: AdminUser) {
        this.id = adminUser.id;
        this.name = adminUser.name;
        this.surname = adminUser.surname;
        this.email = adminUser.email;
    }
}

export class ChangePasswordRequest {
    id: string;
    oldpassword: string;
    newpassword: string;

    constructor(id, oldpassword, newpassword) {
        this.id = id;
        this.oldpassword = oldpassword;
        this.newpassword = newpassword;
    }
}