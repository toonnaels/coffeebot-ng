import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';
import { map, catchError } from 'rxjs/operators';
import { AdminUser } from '../models/admin.model';
import { AdminService } from '../service/admin.service';
import { Router } from '@angular/router';
import { FormControl, Validators, FormBuilder } from '../../../../node_modules/@angular/forms';
import { CurrencyMaskConfig } from '../../../../node_modules/ng2-currency-mask/src/currency-mask.config';
import { HttpConstants } from '../../http/http-constants';
import { CoffeePriceService } from '../service/coffee-price.service';
import { CoffeePrice } from '../models/coffeeprice.model';
import { httpPut } from '../../http/http-handlers';
import { MatDialog } from '@angular/material';
import { ChangePasswordComponent } from './change-password/change-password.component';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {

  admin: AdminUser = new AdminUser();
  prices: CoffeePrice;

  options: CurrencyMaskConfig;

  coffeeprice = new FormControl('', [Validators.required, Validators.min(0.01)]);
  milkprice = new FormControl('', [Validators.required, Validators.min(0.01)]);
  name = new FormControl('', [Validators.required]);
  surname = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);

  loadingSubject = new BehaviorSubject<boolean>(false);

  loading = this.loadingSubject.asObservable();

  constructor(private breakpointObserver: BreakpointObserver, private adminService: AdminService, private coffeePriceService: CoffeePriceService, private router: Router, fb: FormBuilder, private dialog: MatDialog) {
    this.setAdminValues();

    this.prices = new CoffeePrice();
    this.prices.coffee = 0;
    this.prices.milk = 0;
  }

  setAdminValues() {
    this.admin.id = localStorage.getItem(HttpConstants.adminId);
    this.admin.name = localStorage.getItem(HttpConstants.adminName);
    this.admin.surname = localStorage.getItem(HttpConstants.adminSurname);
    this.admin.email = localStorage.getItem(HttpConstants.adminEmail);

    this.name.setValue(this.admin.name);
    this.surname.setValue(this.admin.surname);
    this.email.setValue(this.admin.email);
  }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 1280px)')
  .pipe(
    map(result => result.matches)
  );

  ngOnInit() {
    this.loadCoffeePrices();
  }

  logout() {
    this.adminService.logout();
    this.router.navigate(['/']);
    window.setTimeout(() => window.alert("Successfully logged out!"), 500);
  }

  update() {
    if(this.coffeeprice.valid && this.milkprice.valid) {
      this.loadingSubject.next(true);
      this.prices.coffee = this.coffeeprice.value;
      this.prices.milk = this.milkprice.value;

      this.coffeePriceService.updateCoffeePrice(this.prices).pipe(
        catchError(err => {
          window.alert(err);
          return of({});
        })
      ).subscribe((updatedPrices: CoffeePrice) => {
        this.loadCoffeePrices();
        window.setTimeout(() => window.alert("Coffee prices successfully updated!"), 200);
      });
    }
  }

  updateAdmin() {
    if(this.name.valid && this.surname.valid && this.email.valid) {
      this.loadingSubject.next(true);
      this.admin.name = this.name.value;
      this.admin.surname = this.surname.value;
      this.admin.email = this.email.value;
  
      this.adminService.updateAdminDetails(this.admin).pipe(
        catchError(err => {
          window.alert(err);
          return of({});
        })
      ).subscribe((_) => {
        this.setAdminValues();
        this.loadingSubject.next(false);
        window.setTimeout(() => window.alert("Admin details successfully updated!"), 200);
      });
    }
  }

  loadCoffeePrices() {
    this.loadingSubject.next(true);

    this.coffeePriceService.getCoffeePrices().subscribe((prices) => {
      this.prices = prices;

      this.coffeeprice.setValue(prices.coffee);
      this.milkprice.setValue(prices.milk);
      this.loadingSubject.next(false);
    });
  }

  changePassword() {
    this.dialog.open(ChangePasswordComponent, {
      width: '25em'
    });
  }

  disableCoffeePriceUpdate() {
    return (this.coffeeprice.value === this.prices.coffee && this.milkprice.value === this.prices.milk) || (!this.coffeeprice.valid || !this.milkprice.valid);
  }

  disableAdminUpdate() {
    return (this.name.value === this.admin.name && this.surname.value === this.admin.surname && this.email.value === this.admin.email) || (!this.name.valid || !this.surname.valid || !this.email.valid);
  }

  getErrorMessage(field: string) {
    if(field === 'coffeeprice') {
      return this.coffeeprice.hasError('required') ? 'You must enter a value' : this.coffeeprice.hasError('min') ? 'Price can not be zero' : '';
    } else if(field === 'milkprice') {
      return this.milkprice.hasError('required') ? 'You must enter a password' : this.milkprice.hasError('min') ? 'Price can not be zero' : '';
    } else if(field === 'name') {
      return this.name.hasError('required') ? 'You must enter a name' : '';
    } else if(field === 'surname') {
      return this.name.hasError('required') ? 'You must enter a surname' : '';
    } else if(field === 'email') {
      return this.name.hasError('required') ? 'You must enter an email address' : this.email.hasError('email') ? "You must enter a valid email address" : '';
    }
  }

}
