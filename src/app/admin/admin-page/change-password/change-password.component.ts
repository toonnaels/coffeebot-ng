import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, ValidationErrors } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { of, BehaviorSubject } from 'rxjs';
import { AdminService } from '../../service/admin.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  oldpassword = new FormControl('', [Validators.required]);
  newpassword = new FormControl('', [Validators.required]);
  retypepassword = new FormControl('', [Validators.required, this.passwordsDontMatch()]);

  loadingSubject = new BehaviorSubject<boolean>(false);

  loading = this.loadingSubject.asObservable();

  constructor(public dialogRef: MatDialogRef<ChangePasswordComponent>, private adminService: AdminService) {
  }

  ngOnInit() {
  }

  change() {
    this.loadingSubject.next(true);
    this.retypepassword.updateValueAndValidity();

    if(this.oldpassword.valid && this.newpassword.valid && this.retypepassword.valid) {
      this.adminService.changePassword(this.oldpassword.value, this.newpassword.value).pipe(
        catchError(err => {
          this.loadingSubject.next(false);
          window.alert(err);
          return of({});
        })
      ).subscribe(result => {
        if(result) {
          this.dialogRef.close();
          window.setTimeout(() => window.alert("Successfully updated your password!"), 500);
        } else {
          window.alert("Failed to update password. Maybe the old password was entered incorrectly.");
        }
        this.loadingSubject.next(false);
      });
    }
  }

  passwordsDontMatch(): (control: FormControl) => ValidationErrors {
    return control => {
      if (this.newpassword.value !== control.value) {
        return { 'passwordsDontMatch': true };
      } else {
        return null;
      }
    }
  }

  getErrorMessage(field) {
    if(field === 'oldpassword') {
      return this.oldpassword.hasError('required') ? 'You must enter your old password' : '';
    } else if(field === 'newpassword') {
      return this.newpassword.hasError('required') ? 'You must enter your new password' : '';
    } else if(field === 'retypepassword') {
      return this.retypepassword.hasError('required') ? 'You must re-type your new password' : this.retypepassword.hasError('passwordsDontMatch') ? 'The passwords don\'t match' : '';
    }
  }

}
