import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { AdminService } from './admin.service';
import { HttpConstants } from '../../http/http-constants';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurd implements CanActivateChild {

  constructor(private adminService: AdminService, private router: Router) { }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const loggedIn = localStorage.getItem(HttpConstants.loggedInToken);
    
    // Check for login page specifically.
    if(state.url === '/admin/login' && !loggedIn) {
      return true;
    } else if(state.url === '/admin/login' && loggedIn) {
      this.router.navigate(['/admin']);
      return false;
    }
    
    if(loggedIn) {
      return true;
    }

    this.router.navigate(['/admin/login']);
    return false;
  }
}
