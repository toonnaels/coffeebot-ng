import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginCredentials } from '../models/login-credentials';
import { LoginRequest, ChangePasswordRequest, AdminUserRequest } from '../models/admin-requests';
import { httpPost, httpPut } from '../../http/http-handlers';
import { LoginResponse, ChangePasswordResponse } from '../models/admin-responses';
import { HttpConstants } from '../../http/http-constants';
import { map } from 'rxjs/operators';
import { Login, AdminUser } from '../models/admin.model';
import { Observable } from 'rxjs';
import { User } from '../../models/user-models/user.model';
import { UserResponse } from '../../models/user-models/user-response';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private loginUrl = HttpConstants.serverUrl + 'admin/authenticate';
  private adminUrl = HttpConstants.serverUrl + 'admin';
  private newUserUrl = HttpConstants.serverUrl + 'users';
  private changePasswordUrl = HttpConstants.serverUrl + 'admin/changePassword'

  constructor(private http: HttpClient) { }

  login(credentials: LoginCredentials): Observable<Login> {
    const loginReq = new LoginRequest(credentials.email, credentials.password);
    return httpPost<LoginRequest, LoginResponse>(this.http, this.loginUrl, loginReq, "Failed to login. Please try again later.").pipe(
      map((loginResponse, _) => {
        if(loginResponse.authenticated) {
          localStorage.setItem(HttpConstants.loggedInToken, loginResponse.token);
          this.saveAdminDetails(new AdminUser(loginResponse.admin));
        }

        const login = new Login(loginResponse);
        return login;
      })
    );
  }

  saveAdminDetails(admin: AdminUser) {
    localStorage.setItem(HttpConstants.adminId, admin.id);
    localStorage.setItem(HttpConstants.adminName, admin.name);
    localStorage.setItem(HttpConstants.adminSurname, admin.surname);
    localStorage.setItem(HttpConstants.adminEmail, admin.email);
  }

  logout() {
    localStorage.removeItem(HttpConstants.loggedInToken);
    localStorage.removeItem(HttpConstants.adminName);
    localStorage.removeItem(HttpConstants.adminSurname);
    localStorage.removeItem(HttpConstants.adminEmail);
    localStorage.removeItem(HttpConstants.adminId);
  }

  newUser(user: User): Observable<User> {
    return httpPost<User, UserResponse>(this.http, this.newUserUrl, user, "Could not create user.").pipe(map(userResponse => new User(userResponse)));
  }

  updateAdminDetails(admin: AdminUser): Observable<AdminUser> {
    return httpPut<AdminUserRequest>(this.http, `${this.adminUrl}/${admin.id}`, new AdminUserRequest(admin), "Failed to update admin user details.").pipe(
      map((adminUserResponse: AdminUserRequest) => {
        const admin: AdminUser = new AdminUser();
        admin.id = adminUserResponse.id;
        admin.name = adminUserResponse.name;
        admin.surname = adminUserResponse.surname;
        admin.email = adminUserResponse.email;
        this.saveAdminDetails(admin);
        return admin;
      })
    );
  }

  changePassword(oldPassword: string, newPassword: string): Observable<boolean> {
    const id = localStorage.getItem(HttpConstants.adminId);
    const changePass = new ChangePasswordRequest(id, oldPassword, newPassword)
    return httpPost<ChangePasswordRequest, ChangePasswordResponse>(this.http, `${this.changePasswordUrl}/${id}`, changePass, "Failed to update password.").pipe(
      map((res: ChangePasswordResponse) => res.updated)
    );
  }

}
