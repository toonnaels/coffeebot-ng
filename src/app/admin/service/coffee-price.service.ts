import { Injectable } from '@angular/core';
import { HttpConstants } from '../../http/http-constants';
import { Observable, of } from 'rxjs';
import { CoffeePrice } from '../models/coffeeprice.model';
import { httpGet, httpPut } from '../../http/http-handlers';
import { CoffeeOption, CoffeeOptions } from '../models/coffeeprice-request';
import { map, concatMap, reduce } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })
  export class CoffeePriceService {
      private coffeePricesUrl = HttpConstants.serverUrl + 'coffeeoptions';

      constructor(private http: HttpClient) {}

      getCoffeePrices(): Observable<CoffeePrice> {
        return httpGet<CoffeeOption[]>(this.http, this.coffeePricesUrl, {}, "Failed to fetch coffee prices.").pipe(map((options: CoffeeOption[]) => {
            var coffeeoptions = new CoffeeOptions();
            coffeeoptions.options = options;
            return new CoffeePrice(coffeeoptions);
        }));
      }

      updateCoffeePrice(prices: CoffeePrice): Observable<CoffeePrice> {
          const options = new CoffeeOptions(prices);

          return of(...options.options).pipe(
            concatMap(option => httpPut<CoffeeOption>(this.http, `${this.coffeePricesUrl}/${option.id}`, option, "Failed to update coffee price.")),
            reduce((optionsList: CoffeeOption[], option: CoffeeOption, index: number) => {
                optionsList[index] = option;
                return optionsList;
            }),
            map((optionsList: CoffeeOption[]) => {
                const options: CoffeeOptions = new CoffeeOptions();
                options.options = optionsList
                return new CoffeePrice(options)
            })
          )
      }
  }