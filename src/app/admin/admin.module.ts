import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from '../material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { CustomModule } from '../custom';
import { AuthGaurd } from './service/auth-gaurd.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { NewUserDialogComponent } from './user-management/new-user-dialog/new-user-dialog.component';
import { LoadCreditDialogComponent } from './user-management/load-credit-dialog/load-credit-dialog.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { ChangePasswordComponent } from './admin-page/change-password/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    CustomModule,
    CurrencyMaskModule
  ],
  declarations: [
    LoginComponent,
    AdminPageComponent,
    UserManagementComponent,
    NewUserDialogComponent,
    LoadCreditDialogComponent,
    ChangePasswordComponent
  ],
  providers: [
    AuthGaurd,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ]
})
export class AdminModule { }
