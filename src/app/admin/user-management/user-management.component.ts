import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { UserManagementDataSource } from './user-management-datasource';
import { UserService } from '../../services/user-service/user.service';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { User } from '../../models/user-models/user.model';
import { AdminService } from '../service/admin.service';
import { NewUserDialogComponent } from './new-user-dialog/new-user-dialog.component';
import { LoadCreditDialogComponent } from './load-credit-dialog/load-credit-dialog.component';

@Component({
  selector: 'user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  private static PAGE_SIZE: number = 10;
  private static PAGE_SIZE_OPTIONS: number[] = [5, 10, 20, 50];

  // Deletion
  private usersToDelete: number;
  private usersDeleted: number;

  // Columns displayed in the table. Columns IDs can be added, removed, or reordered.
  displayedColumns = ['selection', 'name', 'studentNumber', 'balance', 'options'];
  // Data source.
  dataSource: UserManagementDataSource;
  // Selection.
  selection: SelectionModel<User>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;
  constructor(private userService: UserService, private adminService: AdminService, private dialog: MatDialog) { }

  ngOnInit() {
    // Setup paginator.
    this.paginator.pageSize = UserManagementComponent.PAGE_SIZE;
    this.paginator.pageSizeOptions = UserManagementComponent.PAGE_SIZE_OPTIONS;

    // Setup datasource.
    this.dataSource = new UserManagementDataSource(this.userService);
    this.dataSource.loadUsers(1, UserManagementComponent.PAGE_SIZE);
    this.dataSource.totalUsers.subscribe(total => this.paginator.length = total);

    // Selection model.
    const initialSelection = [];
    const allowMultiSelect = true;
    this.selection = new SelectionModel<User>(allowMultiSelect, initialSelection);
  }

  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, 'keyup').pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadUsersPage();
      })
    ).subscribe();

    // Reset the paginator after sorting.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadUsersPage())
    ).subscribe();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.users.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.users.forEach(row => this.selection.select(row));
  }

  loadUsersPage() {
    this.dataSource.loadUsers(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction, this.input.nativeElement.value);
    this.selection.clear();
  }

  newUser() {
    const dialogRef = this.dialog.open(NewUserDialogComponent, {
      width: '25em',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(_ => {
      this.loadUsersPage();
    });
  }

  deleteUsers() {
    if(window.confirm(`Are you sure you want to delete the selected user${(this.selection.selected.length > 1) ? 's' : ''}?`)) {
      this.usersToDelete = this.selection.selected.length;
      this.usersDeleted = 0;

      this.dataSource.deleteUsers(this.selection.selected).subscribe(success => {
        this.usersDeleted += 1;

        if(this.usersDeleted === this.usersToDelete) {
          this.loadUsersPage();
        } else if(!success) {
          this.loadUsersPage();
          window.alert(`Failed to delete the selected user${(this.usersToDelete > 1) ? 's' : ''}.`);
        }
      });
    }
  }

  loadCredit(user: User) {
    const dialogRef = this.dialog.open(LoadCreditDialogComponent, {
      width: '25em',
      disableClose: true,
      data: user
    });

    dialogRef.afterClosed().subscribe(_ => {
      this.loadUsersPage();
    });
  }
}
