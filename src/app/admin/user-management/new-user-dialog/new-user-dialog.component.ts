import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user-service/user.service';
import { FormControl, Validators, ValidationErrors } from '@angular/forms';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { UnregisteredUser } from '../../../models/unregistered-user-models/unregistered-user.model';
import { UnregisteredUserService } from '../../../services/unregistered-user-service/unregistered-user.service';
import { map, catchError } from 'rxjs/operators';
import { User } from '../../../models/user-models/user.model';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-new-user-dialog',
  templateUrl: './new-user-dialog.component.html',
  styleUrls: ['./new-user-dialog.component.scss']
})
export class NewUserDialogComponent implements OnInit {
  name = new FormControl('', [Validators.required]);
  surname = new FormControl('', [Validators.required]);
  studentNumber = new FormControl('', [Validators.required, Validators.maxLength(8), Validators.minLength(8), Validators.pattern('^[0-9]+$')], [this.studentNumberExists(this.userService)]);
  cardUUID = new FormControl('', [Validators.required]);
  balance = new FormControl('', [Validators.required])

  unregisteredUsersSubject = new BehaviorSubject<UnregisteredUser[]>([]);
  loadingSubject = new BehaviorSubject<boolean>(false);

  unregisteredUsers = this.unregisteredUsersSubject.asObservable();
  loading = this.loadingSubject.asObservable();

  selectedUnregisteredUser: UnregisteredUser;

  constructor(public dialogRef: MatDialogRef<NewUserDialogComponent>, private userService: UserService, private unregUserservice: UnregisteredUserService) { }

  ngOnInit() {
    this.loadUnregisteredUsers();
  }

  getErrorMessage(field) {
    if(field === 'name') {
      return this.name.hasError('required') ? 'You must enter a value' : '';
    } else if(field === 'surname') {
      return this.surname.hasError('required') ? 'You must enter a value' : '';
    } else if(field === 'studentNumber') {
      return this.studentNumber.hasError('required') ? 'You must enter a value' : (this.studentNumber.hasError('maxlength') || this.studentNumber.hasError('minlength')) ? 'Must be 8 characters' : this.studentNumber.hasError('pattern') ? 'Should consist of numbers only' : this.studentNumber.hasError('studentNumberExists') ? 'Student number already exists' : '';
    } else if(field === 'cardUUID') {
      return this.cardUUID.hasError('required') ? 'You must select a value' : '';
    }else if(field === 'balance') {
      return this.balance.hasError('required') ? 'You must enter a value' : '';
    }
  }

  studentNumberExists(userService: UserService): (control: FormControl) => Observable<ValidationErrors> {
    return control => {
      return userService.getUserByStudentNumber(control.value).pipe(map(user => {
        if(user.name && user.surname && user.studentNumber) {
          return {"studentNumberExists": true};
        } else {
          return null;
        }
      }));
    }
  }

  loadUnregisteredUsers() {
    this.loadingSubject.next(true);

    this.unregUserservice.getUnregisteredUsers().subscribe(users => {
      this.loadingSubject.next(false);
      this.unregisteredUsersSubject.next(users);
    });
  }

  addUser() {
    if(!this.name.invalid && !this.surname.invalid && !this.studentNumber.invalid && !this.cardUUID.invalid && !this.balance.invalid) {
      this.loadingSubject.next(true);

      const newUser = new User();
      newUser.name = this.name.value;
      newUser.surname = this.surname.value;
      newUser.studentNumber = this.studentNumber.value;
      newUser.cardUUID = this.cardUUID.value.cardUUID;
      newUser.balance = this.balance.value;

      this.userService.saveUser(newUser).pipe(
        catchError(err => {
          window.alert(err);
          return of({});
        })
      ).subscribe(success => {
        this.loadingSubject.next(false);

        if(success === true) {
          this.dialogRef.close();
        }
      })
    }
  }
}
