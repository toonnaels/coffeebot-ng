import { DataSource } from '@angular/cdk/collections';
import { finalize, concatMap, catchError, map } from 'rxjs/operators';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { User } from '../../models/user-models/user.model';
import { UserService } from '../../services/user-service/user.service';

/**
 * Data source for the UserManagement view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class UserManagementDataSource extends DataSource<User> {
  private usersSubject = new BehaviorSubject<User[]>([]);
  private totalUsersSubject = new BehaviorSubject<number>(0);
  private errorSubject = new BehaviorSubject<string>('');
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public users: User[];

  public totalUsers = this.totalUsersSubject.asObservable();
  public error = this.errorSubject.asObservable();
  public loading = this.loadingSubject.asObservable();

  constructor(private userService: UserService) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<User[]> {
    return this.usersSubject.asObservable();
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.usersSubject.complete();
    this.loadingSubject.complete();
    this.errorSubject.complete();
    this.totalUsersSubject.complete();
  }

  /**
   * Load the users to display.
   * 
   * @param pageIndex the index of the current page to display.
   * @param pageSize the size of a page.
   * @param sort the sorting parameters.
   * @param filter the filter parameters.
   */
  loadUsers(pageIndex: number, pageSize: number = 10, sortField: string = 'name', sortDir: string = 'asc', filter: string = '') {
    this.loadingSubject.next(true);

    this.userService.getUsers(pageIndex, pageSize, sortField, sortDir, filter).pipe(
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(
      users => {
        this.totalUsersSubject.next(users.totalRecords);
        this.users = users.response;
        this.usersSubject.next(users.response);
      },
      error => {
        this.errorSubject.next(error);
        this.usersSubject.next([]);
      }
    );
  }

  deleteUsers(users: User[]): Observable<boolean> {
    this.loadingSubject.next(true);
    return of(...users).pipe(
      concatMap(user => this.userService.deleteUser(user)),
      map(_ => true),
      catchError(err => of(false))
    );
  }
}