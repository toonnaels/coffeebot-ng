import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User, LoadCredit } from '../../../models/user-models/user.model';
import { FormControl, Validators, ValidationErrors } from '@angular/forms';
import { BehaviorSubject, of } from 'rxjs';
import { UserService } from '../../../services/user-service/user.service';

@Component({
  selector: 'app-load-credit-dialog',
  templateUrl: './load-credit-dialog.component.html',
  styleUrls: ['./load-credit-dialog.component.scss']
})
export class LoadCreditDialogComponent implements OnInit {
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading = this.loadingSubject.asObservable();

  amount = new FormControl('', [Validators.required, this.isZero()]);

  user: User;

  constructor(public dialogRef: MatDialogRef<LoadCreditDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: User, private userService: UserService ) {
    this.user = data;
  }

  ngOnInit() {
  }

  getErrorMessage(field) {
    if(field === 'amount') {
      return this.amount.hasError('required') ? 'You must enter a value' :  '';
    }
  }

  isZero(): (control: FormControl) => ValidationErrors {
    return control => {
      if(control.value === 0) {
        return {"isZero": true};
      } else {
        return null;
      }
    }
  }

  loadUserCredit() {
    if(!this.amount.invalid) {
      this.loadingSubject.next(true);

      const loadCredit = new LoadCredit(this.user.id, +this.amount.value);

      this.userService.loadCredit(loadCredit).subscribe(_ => {
        this.loadingSubject.next(false);
        this.dialogRef.close();
      });
    }
  }

}
