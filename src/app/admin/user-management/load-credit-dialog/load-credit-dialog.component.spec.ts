import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadCreditDialogComponent } from './load-credit-dialog.component';

describe('LoadCreditDialogComponent', () => {
  let component: LoadCreditDialogComponent;
  let fixture: ComponentFixture<LoadCreditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadCreditDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadCreditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
