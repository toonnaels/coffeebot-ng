import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { AuthGaurd } from './service/auth-gaurd.service';

const adminRoutes : Routes = [
  { path: 'admin',
    children: [
      {
        path: '',
        canActivateChild: [AuthGaurd],
        children: [
          { path: 'login', component: LoginComponent },
          { path: 'management', component: AdminPageComponent },
          { path: '', redirectTo:'management', pathMatch:'full' }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
