import { Injectable } from '@angular/core';
import { LogItem } from '../../models/coffeelog-models/log-item.model';
import { PurchaseResponse } from '../../models/coffeelog-models/purchase-response';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PaginatedResponse } from '../../http/pagination-result.model';
import { httpGet } from '../../http/http-handlers';
import { map } from 'rxjs/operators';
import { HttpConstants } from '../../http/http-constants';

@Injectable({
  providedIn: 'root'
})
export class CoffeelogService {
  private getAllUrl: string = HttpConstants.serverUrl + 'purchases/populate';

  constructor(private http: HttpClient) { }

  /**
   * Obtains the coffee logs for the requested page.
   */
  getLogs(pageIndex: number, logsPerPage: number, sortField: string, sortDir: string, filter: string): Observable<PaginatedResponse<LogItem>> {
    let sortString = sortField;
    if(sortDir === 'desc') {
      sortString = `-${sortString}`;
    }

    let params = new HttpParams().set('sort', sortString).append('page', pageIndex.toString()).append('pageSize', logsPerPage.toString());
    if(filter) {
      params = params.append('filter', filter.replace(' ', ','));
    }
    const options = {params: params};

    return httpGet(this.http, this.getAllUrl, options, "Failed to fetch data, please try again later.").pipe(
      map((response: PaginatedResponse<PurchaseResponse>, _) => {
        const logs: LogItem[] = response.response.map((logResponse) => new LogItem(logResponse));
        return new PaginatedResponse<LogItem>(response.page, response.totalPages, response.totalRecords, logs);
      })
    );
  }
}
