import { TestBed, inject } from '@angular/core/testing';

import { CoffeelogService } from './coffeelog.service';

describe('CoffeelogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoffeelogService]
    });
  });

  it('should be created', inject([CoffeelogService], (service: CoffeelogService) => {
    expect(service).toBeTruthy();
  }));
});
