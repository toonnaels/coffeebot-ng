import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { httpGet, httpPost, httpDelete } from '../../http/http-handlers';
import { PaginatedResponse } from '../../http/pagination-result.model';
import { UserResponse, TopDrinkersResponse, TopDebtorsResponse } from '../../models/user-models/user-response';
import { map } from 'rxjs/operators';
import { User, TopDrinkers, TopDebtors, LoadCredit } from '../../models/user-models/user.model';
import { HttpConstants } from '../../http/http-constants';
import { UserRequest, LoadCreditRequest } from '../../models/user-models/user-request';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersUrl: string = HttpConstants.serverUrl + 'users';
  private registerUserUrl: string = HttpConstants.serverUrl + 'users/register'
  private getTopDrinkersUrl: string = HttpConstants.serverUrl + 'users/topDrinkers';
  private getTopMilkUrl: string = HttpConstants.serverUrl + 'users/topMilk';
  private getTopDebtorsUrl: string = HttpConstants.serverUrl + 'users/topDebtors';
  private getTopPayersUrl: string = HttpConstants.serverUrl + 'users/topPayers';
  private getUserByStudentNumberUrl: string = HttpConstants.serverUrl + 'users/studentNumber/';
  private loadCreditUrl: string = HttpConstants.serverUrl + 'users/loadCredit';

  constructor(private http: HttpClient) { }

  /**
   * Obtains the users for the requested page.
   */
  getUsers(pageIndex: number, usersPerPage: number, sortField: string, sortDir: string, filter: string): Observable<PaginatedResponse<User>> {
    let sortString = sortField;
    if(sortDir === 'desc') {
      sortString = `-${sortString}`;
    }

    let params = new HttpParams().set('sort', sortString).append('page', pageIndex.toString()).append('pageSize', usersPerPage.toString());
    if(filter) {
      params = params.append('filter', filter.replace(' ', ','));
    }
    const options = {params: params};

    return httpGet(this.http, this.usersUrl, options, "Failed to fetch data, please try again later.").pipe(
      map((response: PaginatedResponse<UserResponse>, _) => {
        const users: User[] = response.response.map((userResponse) => new User(userResponse));
        return new PaginatedResponse<User>(response.page, response.totalPages, response.totalRecords, users);
      })
    );
  }

  /**
   * Obtains the top coffee drinkers.
   */
  getTopDrinkers(): Observable<TopDrinkers> {
    return httpGet<TopDrinkersResponse>(this.http, this.getTopDrinkersUrl, {}, "Failed to fetch data, please try again later.").pipe(
      map((response: TopDrinkersResponse, _) => {
        return new TopDrinkers(response);
      })
    );
  }

  /**
   * Obtains the top milk drinkers.
   */
  getTopMilk(): Observable<TopDrinkers> {
    return httpGet<TopDrinkersResponse>(this.http, this.getTopMilkUrl, {}, "Failed to fetch data, please try again later.").pipe(
      map((response: TopDrinkersResponse, _) => {
        return new TopDrinkers(response);
      })
    );
  }

  /**
   * Obtain the top users in debt.
   */
  getTopDebtors(): Observable<TopDebtors> {
    return httpGet<TopDebtorsResponse>(this.http, this.getTopDebtorsUrl, {}, "Failed to fetch data, please try again later.").pipe(
      map((response: TopDebtorsResponse, _) => {
        return new TopDebtors(response);
      })
    );
  }

/**
  * Obtain the top paying users.
  */
 getTopPayers(): Observable<TopDebtors> {
   return httpGet<TopDebtorsResponse>(this.http, this.getTopPayersUrl, {}, "Failed to fetch data, please try again later.").pipe(
     map((response: TopDebtorsResponse, _) => {
       return new TopDebtors(response);
     })
   );
 }

  /**
   * Fetch a user by searching for his/her student number.
   * @param studentNumber The studentNumber of the user to fetch.
   */
  getUserByStudentNumber(studentNumber: string): Observable<User> {
    return httpGet<UserResponse>(this.http, this.getUserByStudentNumberUrl + studentNumber, {}, `Could not fetch user with student number ${studentNumber}`).pipe(
      map(response => {
        if(response.name && response.surname && response.studentNumber) {
          return new User(response)
        } else {
          return new User();
        }
      })
    );
  }

  /**
   * Save a new user.
   * @param user the user to save.
   */
  saveUser(user: User): Observable<boolean> {
    return httpPost<UserRequest, UserResponse>(this.http, this.registerUserUrl, new UserRequest(user), "Failed to add the user.").pipe(
      map(userResp => {
        if(userResp.name && userResp.surname && userResp.studentNumber) {
          return true;
        } else {
          return false;
        }
      })
    );
  }

  /**
   * Delete the requested user.
   * @param user the user to delete.
   */
  deleteUser(user:User): Observable<{}> {
    return httpDelete(this.http, `${this.usersUrl}/${user.id}`, "Failed to delete user.");
  }

  /**
   * Load credit for the specified user.
   * @param loadCredit the amount of credit to load for the user with the given id.
   */
  loadCredit(loadCredit: LoadCredit): Observable<User> {
    return httpPost<LoadCreditRequest, UserResponse>(this.http, this.loadCreditUrl, new LoadCreditRequest(loadCredit), "Failed to load credit for the user.").pipe(
      map(userResponse => new User(userResponse))
    );
  }
}
