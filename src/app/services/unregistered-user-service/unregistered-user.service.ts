import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UnregisteredUser } from '../../models/unregistered-user-models/unregistered-user.model';
import { httpGet } from '../../http/http-handlers';
import { UnregisteredUserResponse } from '../../models/unregistered-user-models/unregistered-user-response';
import { map } from 'rxjs/operators';
import { HttpConstants } from '../../http/http-constants';

@Injectable({
  providedIn: 'root'
})
export class UnregisteredUserService {
  private getUnregisteredUsersUrl = HttpConstants.serverUrl + 'unregisteredUsers';

  constructor(private http: HttpClient) { }

  getUnregisteredUsers(): Observable<UnregisteredUser[]> {
    return httpGet<UnregisteredUserResponse[]>(this.http, this.getUnregisteredUsersUrl, {}, "Could not fetch card UUID's of new users.").pipe(
      map(response => response.map(userResponse => new UnregisteredUser(userResponse)))
    );
  }
}
