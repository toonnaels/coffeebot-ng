import { TestBed, inject } from '@angular/core/testing';

import { UnregisteredUserService } from './unregistered-user.service';

describe('UnregisteredUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnregisteredUserService]
    });
  });

  it('should be created', inject([UnregisteredUserService], (service: UnregisteredUserService) => {
    expect(service).toBeTruthy();
  }));
});
