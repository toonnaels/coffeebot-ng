import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { httpGet } from '../../http/http-handlers';
import { PaginatedResponse } from '../../http/pagination-result.model';
import { map } from 'rxjs/operators';
import { HttpConstants } from '../../http/http-constants';
import { Activity } from '../../models/activity-models/activity.model';
import { ActivityResponse } from '../../models/activity-models/activity-response';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  private activitiesUrl: string = HttpConstants.serverUrl + 'activities';

  constructor(private http: HttpClient) { }

  /**
   * Obtains the activities for the requested page.
   */
  getActivities(pageIndex: number, activitiesPerPage: number, sortField: string, sortDir: string, filter: string): Observable<PaginatedResponse<Activity>> {
    let sortString = sortField;
    if(sortDir === 'desc') {
      sortString = `-${sortString}`;
    }

    let params = new HttpParams().set('sort', sortString).append('page', pageIndex.toString()).append('pageSize', activitiesPerPage.toString());
    if(filter) {
      params = params.append('filter', filter.replace(' ', ','));
    }
    const options = {params: params};

    return httpGet(this.http, this.activitiesUrl, options, "Failed to fetch data, please try again later.").pipe(
      map((response: PaginatedResponse<ActivityResponse>, _) => {
        const activities: Activity[] = response.response.map((activityResponse) => new Activity(activityResponse));
        return new PaginatedResponse<Activity>(response.page, response.totalPages, response.totalRecords, activities);
      })
    );
  }
}
