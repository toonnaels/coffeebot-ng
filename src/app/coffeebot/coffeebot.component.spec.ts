import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffeebotComponent } from './coffeebot.component';

describe('CoffeebotComponent', () => {
  let component: CoffeebotComponent;
  let fixture: ComponentFixture<CoffeebotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffeebotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffeebotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
