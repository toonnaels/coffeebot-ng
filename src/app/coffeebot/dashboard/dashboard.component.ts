import { Component, OnInit } from '@angular/core';
import { TopCoffeeDrinkersCardConstants } from '../display-card/top-drinkers/top-drinkers';
import { TopMilkDrinkersCardConstants } from '../display-card/top-milk/top-milk';
import { TopDebtorsCardConstants } from '../display-card/top-debtors/top-debtors';
import { CoffeelogCardConstants } from '../coffeelog/coffeelog-card/coffeelog-card';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private static COL_WIDTH_TRANSITION: number = 1340;

  topDrinkersCardConstants = new TopCoffeeDrinkersCardConstants();
  topMilkCardConstants = new TopMilkDrinkersCardConstants();
  topDebtorsCardConstants = new TopDebtorsCardConstants();
  coffeelogCardConstants = new CoffeelogCardConstants();

  breakpoint: number;

  ngOnInit() {
    this.breakpoint = (window.innerWidth <= DashboardComponent.COL_WIDTH_TRANSITION) ? 1 : 3;
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= DashboardComponent.COL_WIDTH_TRANSITION) ? 1 : 3;
  }

}
