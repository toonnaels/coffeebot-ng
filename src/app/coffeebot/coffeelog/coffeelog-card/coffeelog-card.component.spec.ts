import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffeelogCardComponent } from './coffeelog-card.component';

describe('CoffeelogCardComponent', () => {
  let component: CoffeelogCardComponent;
  let fixture: ComponentFixture<CoffeelogCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffeelogCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffeelogCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
