import { Component, OnInit } from '@angular/core';
import { CoffeelogCardConstants } from './coffeelog-card';

@Component({
  selector: 'app-coffeelog-card',
  templateUrl: './coffeelog-card.component.html',
  styleUrls: ['./coffeelog-card.component.scss']
})
export class CoffeelogCardComponent implements OnInit {
  coffeelogCardConstants = new CoffeelogCardConstants();

  constructor() { }

  ngOnInit() {
  }

}
