import { Colors } from '../../../custom/colors/colors';

export class CoffeelogCardConstants {
  imageUrl: string = '../../assets/images/coffee.svg';
  title: string = 'Coffee Log';
  backgroundColor: string = Colors.BROWN_LIGHT;
  imageBackgroundColor: string = Colors.BROWN_DARK;
}