
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffeelogTableComponent } from './coffeelog-table.component';

describe('CoffeelogTableComponent', () => {
  let component: CoffeelogTableComponent;
  let fixture: ComponentFixture<CoffeelogTableComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffeelogTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoffeelogTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
