import { DataSource } from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { LogItem } from '../../../models/coffeelog-models/log-item.model';
import { CoffeelogService } from '../../../services/coffeelog-service/coffeelog.service';
import { MatDialog } from '@angular/material';

/**
 * Data source for the CoffeelogTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class CoffeelogDataSource extends DataSource<LogItem> {
  private coffeelogSubject = new BehaviorSubject<LogItem[]>([]);
  private totalLogsSubject = new BehaviorSubject<number>(0);
  private errorSubject = new BehaviorSubject<string>('');
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public totalLogs = this.totalLogsSubject.asObservable();
  public error = this.errorSubject.asObservable();
  public loading = this.loadingSubject.asObservable();

  constructor(private coffeelogService: CoffeelogService, private dialog: MatDialog) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<LogItem[]> {
    return this.coffeelogSubject.asObservable();
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.coffeelogSubject.complete();
    this.loadingSubject.complete();
    this.errorSubject.complete();
    this.totalLogsSubject.complete();
  }

  showLoading() {
    this.loadingSubject.next(true);
  }

  /**
   * Load the logs to display.
   * 
   * @param pageIndex the index of the current page to display.
   * @param pageSize the size of a page.
   * @param sort the sorting parameters.
   * @param filter the filter parameters.
   */
  loadLogs(pageIndex: number, pageSize: number, sortField: string = 'date', sortDir: string = 'desc', filter: string = '') {
    this.coffeelogService.getLogs(pageIndex, pageSize, sortField, sortDir, filter).pipe(
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(
      logs => {
        this.totalLogsSubject.next(logs.totalRecords);
        this.coffeelogSubject.next(logs.response);
        this.errorSubject.next('');
      },
      error => {
        this.errorSubject.next(error);
        this.totalLogsSubject.next(0);
        this.coffeelogSubject.next([]);
      }
    );
  }
}