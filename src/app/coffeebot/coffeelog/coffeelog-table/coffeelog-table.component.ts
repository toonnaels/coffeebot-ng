import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { CoffeelogDataSource } from './coffeelog-table-datasource';
import { CoffeelogService } from '../../../services/coffeelog-service/coffeelog.service';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

@Component({
  selector: 'coffeelog-table',
  templateUrl: './coffeelog-table.component.html',
  styleUrls: ['./coffeelog-table.component.scss']
})
export class CoffeelogTableComponent implements OnInit, AfterViewInit {
  private static PAGE_SIZE: number = 10;
  private static PAGE_SIZE_OPTIONS: number[] = [5, 10, 20, 50];

  // Columns displayed in the table. Columns IDs can be added, removed, or reordered.
  displayedColumns = ['name', 'date', 'item', 'price'];
  // Data source.
  dataSource: CoffeelogDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;
  constructor(private coffeeLogService: CoffeelogService, private dialog: MatDialog) { }

  ngOnInit() {
    // Setup paginator.
    this.paginator.pageSize = CoffeelogTableComponent.PAGE_SIZE;
    this.paginator.pageSizeOptions = CoffeelogTableComponent.PAGE_SIZE_OPTIONS;

    // Setup datasource.
    this.dataSource = new CoffeelogDataSource(this.coffeeLogService, this.dialog);
    this.dataSource.showLoading();
    this.dataSource.loadLogs(1, CoffeelogTableComponent.PAGE_SIZE);
    this.dataSource.totalLogs.subscribe(total => this.paginator.length = total);

    setInterval(() => {
      this.loadCoffeelogPage();
    }, 5000);
  }

  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, 'keyup').pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadCoffeelogPage();
      })
    ).subscribe();

    // Reset the paginator after sorting.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadCoffeelogPage())
    ).subscribe();
  }

  loadCoffeelogPage() {
    this.dataSource.loadLogs(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction, this.input.nativeElement.value);
  }
}
