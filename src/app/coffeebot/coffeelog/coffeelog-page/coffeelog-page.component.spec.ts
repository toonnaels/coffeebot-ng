import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoffeelogPageComponent } from './coffeelog-page.component';

describe('CoffeelogPageComponent', () => {
  let component: CoffeelogPageComponent;
  let fixture: ComponentFixture<CoffeelogPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoffeelogPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoffeelogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
