import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UsersDataSource } from './users-datasource';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { UserService } from '../../../services/user-service/user.service';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {
  private static PAGE_SIZE: number = 10;
  private static PAGE_SIZE_OPTIONS: number[] = [5, 10, 20, 50];

  // Columns displayed in the table. Columns IDs can be added, removed, or reordered.
  displayedColumns = ['name', 'studentNumber', 'balance'];
  // Data source.
  dataSource: UsersDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;
  constructor(private userService: UserService, private dialog: MatDialog) { }

  ngOnInit() {
    // Setup paginator.
    this.paginator.pageSize = UserTableComponent.PAGE_SIZE;
    this.paginator.pageSizeOptions = UserTableComponent.PAGE_SIZE_OPTIONS;

    // Setup datasource.
    this.dataSource = new UsersDataSource(this.userService, this.dialog);
    this.dataSource.loadUsers(1, UserTableComponent.PAGE_SIZE);
    this.dataSource.totalUsers.subscribe(total => this.paginator.length = total);
  }

  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, 'keyup').pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadUsersPage();
      })
    ).subscribe();

    // Reset the paginator after sorting.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadUsersPage())
    ).subscribe();
  }

  loadUsersPage() {
    this.dataSource.loadUsers(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction, this.input.nativeElement.value);
  }
}
