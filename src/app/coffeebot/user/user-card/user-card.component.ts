import { Component, OnInit } from '@angular/core';
import { UserCardConstants } from './user-card';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
  userCardConstants = new UserCardConstants();
  
  constructor() { }

  ngOnInit() {
  }

}
