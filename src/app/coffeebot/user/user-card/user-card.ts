import { Colors } from "../../../custom/colors/colors";

export class UserCardConstants {
  imageUrl = '../../assets/images/person.svg';
  title = 'Users';
  backgroundColor = Colors.BLUE_LIGHT;
  imageBackgroundColor = Colors.BLUE_DARK;
}