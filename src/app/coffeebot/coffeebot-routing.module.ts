import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersPageComponent } from './user/users-page/users-page.component';
import { CoffeelogPageComponent } from './coffeelog/coffeelog-page/coffeelog-page.component';
import { CoffeebotComponent } from './coffeebot.component';
import { RecentActivityPageComponent } from './recent-activity/recent-activity-page/recent-activity-page.component';

const coffeebotRoutes: Routes = [
  {
    path: '',
    component: CoffeebotComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'dashboard', component: DashboardComponent},
          { path: 'users', component: UsersPageComponent },
          { path: 'coffeelog', component: CoffeelogPageComponent },
          { path: 'recent-activity', component: RecentActivityPageComponent },
          { path: '', redirectTo:'dashboard', pathMatch:'full'}
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(coffeebotRoutes)],
  exports: [RouterModule]
})
export class CoffeebotRoutingModule { }
