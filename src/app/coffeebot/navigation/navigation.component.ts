import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  menuTitle: string = "";

  links = [
    { name: "Dashboard", ref: "/dashboard" },
    { name: "Users", ref: "/users" },
    { name: "Coffee Log", ref: "/coffeelog" },
    { name: "Recent Activity", ref: "/recent-activity" }
  ];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
    
  constructor(private breakpointObserver: BreakpointObserver) { }
}
