import { Component, OnInit } from '@angular/core';
import { RecentActivityCardConstants } from './recent-activity-card';

@Component({
  selector: 'app-recent-activity-card',
  templateUrl: './recent-activity-card.component.html',
  styleUrls: ['./recent-activity-card.component.scss']
})
export class RecentActivityCardComponent implements OnInit {
  recentActivityCardConstants = new RecentActivityCardConstants();

  constructor() { }

  ngOnInit() {
  }

}
