import { Colors } from '../../../custom/colors/colors';

export class RecentActivityCardConstants {
  imageUrl: string = '../../assets/images/activity.svg';
  title: string = 'Recent Activity';
  backgroundColor: string = Colors.GREEN_LIGHT;
  imageBackgroundColor: string = Colors.GREEN_DARK;
}