import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { RecentActivityDataSource } from './recent-activity-table-datasource';
import { fromEvent, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { ActivityService } from '../../../services/activity-service/activity.service';

@Component({
  selector: 'recent-activity-table',
  templateUrl: './recent-activity-table.component.html',
  styleUrls: ['./recent-activity-table.component.scss']
})
export class RecentActivityTableComponent implements OnInit, AfterViewInit {
  private static PAGE_SIZE: number = 10;
  private static PAGE_SIZE_OPTIONS: number[] = [5, 10, 20, 50];

  // Columns displayed in the table. Columns IDs can be added, removed, or reordered.
  displayedColumns = ['date', 'description'];
  // Data source.
  dataSource: RecentActivityDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;
  constructor(private activityService: ActivityService, private dialog: MatDialog) { }

  ngOnInit() {
    // Setup paginator.
    this.paginator.pageSize = RecentActivityTableComponent.PAGE_SIZE;
    this.paginator.pageSizeOptions = RecentActivityTableComponent.PAGE_SIZE_OPTIONS;

    // Setup datasource.
    this.dataSource = new RecentActivityDataSource(this.activityService, this.dialog);
    this.dataSource.loadActivities(1, RecentActivityTableComponent.PAGE_SIZE);
    this.dataSource.totalActivities.subscribe(total => this.paginator.length = total);
  }

  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, 'keyup').pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadActivityPage();
      })
    ).subscribe();

    // Reset the paginator after sorting.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadActivityPage())
    ).subscribe();
  }

  loadActivityPage() {
    this.dataSource.loadActivities(this.paginator.pageIndex + 1, this.paginator.pageSize, this.sort.active, this.sort.direction, this.input.nativeElement.value);
  }
}
