import { DataSource } from '@angular/cdk/collections';
import { finalize } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { ActivityService } from '../../../services/activity-service/activity.service';
import { Activity } from '../../../models/activity-models/activity.model';

/**
 * Data source for the RecentActivityDataSourceTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class RecentActivityDataSource extends DataSource<Activity> {
  private recentActivitySubject = new BehaviorSubject<Activity[]>([]);
  private totalActivitiesSubject = new BehaviorSubject<number>(0);
  private errorSubject = new BehaviorSubject<string>('');
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public totalActivities = this.totalActivitiesSubject.asObservable();
  public error = this.errorSubject.asObservable();
  public loading = this.loadingSubject.asObservable();

  constructor(private activityService: ActivityService, private dialog: MatDialog) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<Activity[]> {
    return this.recentActivitySubject.asObservable();
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.recentActivitySubject.complete();
    this.loadingSubject.complete();
    this.errorSubject.complete();
    this.totalActivitiesSubject.complete();
  }

  /**
   * Load the logs to display.
   * 
   * @param pageIndex the index of the current page to display.
   * @param pageSize the size of a page.
   * @param sort the sorting parameters.
   * @param filter the filter parameters.
   */
  loadActivities(pageIndex: number, pageSize: number, sortField: string = 'date', sortDir: string = 'desc', filter: string = '') {
    this.loadingSubject.next(true);

    this.activityService.getActivities(pageIndex, pageSize, sortField, sortDir, filter).pipe(
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(
      activities => {
        this.totalActivitiesSubject.next(activities.totalRecords);
        this.recentActivitySubject.next(activities.response);
      },
      error => {
        this.errorSubject.next(error);
      }
    );
  }
}