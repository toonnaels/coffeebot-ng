import { CoffeebotModule } from './coffeebot.module';

describe('CoffeebotModule', () => {
  let coffeebotModule: CoffeebotModule;

  beforeEach(() => {
    coffeebotModule = new CoffeebotModule();
  });

  it('should create an instance', () => {
    expect(coffeebotModule).toBeTruthy();
  });
});
