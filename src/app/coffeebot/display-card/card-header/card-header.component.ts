import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-header',
  templateUrl: './card-header.component.html',
  styleUrls: ['./card-header.component.scss']
})
export class CardHeaderComponent implements OnInit {

  @Input() image: string;
  @Input() title: string;
  @Input() backgroundColor: string;
  @Input() imageBackgroundColor: string;

  constructor() { }

  ngOnInit() {
  }

}
