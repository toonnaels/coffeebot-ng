import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopMilkComponent } from './top-milk.component';

describe('TopDrinkersComponent', () => {
  let component: TopMilkComponent;
  let fixture: ComponentFixture<TopMilkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopMilkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopMilkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
