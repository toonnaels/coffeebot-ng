import { Component, OnInit } from '@angular/core';
import { TopMilkDrinkersCardConstants } from './top-milk';
import { BehaviorSubject, of } from 'rxjs';
import { TopDrinkers, TopDrinker } from '../../../models/user-models/user.model';
import { UserService } from '../../../services/user-service/user.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-top-milk',
  templateUrl: './top-milk.component.html',
  styleUrls: ['./top-milk.component.scss']
})
export class TopMilkComponent implements OnInit {
  private topMilkSubject = new BehaviorSubject<TopDrinker[]>(null);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  topMilkCardConstants = new TopMilkDrinkersCardConstants();
  loading = this.loadingSubject.asObservable();

  topMilkData: TopDrinkers;
  topMilk = this.topMilkSubject.asObservable();

  timeFrames = ['day', 'week', 'month', 'year'];
  defaultTimeFrame = this.timeFrames[2];
  selectedTime: string;

  error: string;
  message: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.selectedTime = this.defaultTimeFrame;

    this.loadingSubject.next(true);
    this.refreshData();
    setInterval(() => {
      this.refreshData()
    }, 5000);
  }

  refreshData() {
    this.userService.getTopMilk().pipe(
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(
      topMilk => {
        this.topMilkData = topMilk;

        if(topMilk[this.selectedTime].length != 0) {
          this.topMilkSubject.next(topMilk[this.selectedTime]);
          this.message = '';
        } else {
          this.topMilkSubject.next([]);
          this.updateMessage();
        }

        this.error = null;
      },
      error => {
        this.topMilkSubject.next([]);
        this.error = error;
      }
    );
  }

  changeFilter() {
    if(this.topMilkData[this.selectedTime].length != 0) {
      this.topMilkSubject.next(this.topMilkData[this.selectedTime]);
      this.message = '';
    } else {
      this.topMilkSubject.next([]);
      this.updateMessage();
    }
  }

  updateMessage() {
    this.message = `Nobody bought milk ${(this.selectedTime == this.timeFrames[0]) ? "today" : "this " + this.selectedTime}. You poor people...`;
  }

}
