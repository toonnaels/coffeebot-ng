import { Colors } from "../../../custom/colors/colors";

export class TopMilkDrinkersCardConstants {
  imageUrl = '../../assets/images/milk.svg';
  title = 'Milk Men';
  backgroundColor = Colors.SILVER_LIGHT;
  imageBackgroundColor = Colors.SILVER_DARK;
}