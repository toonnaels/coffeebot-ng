import { Colors } from "../../../custom/colors/colors";

export class TopDebtorsCardConstants {
    imageUrl = '../../assets/images/coin.svg';
    title = 'Name and Shame';
    backgroundColor = Colors.RED_LIGHT;
    imageBackgroundColor = Colors.RED_DARK;
  }