import { Component, OnInit } from '@angular/core';
import { TopDebtorsCardConstants } from './top-debtors';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../../models/user-models/user.model';
import { UserService } from '../../../services/user-service/user.service';
import { finalize } from 'rxjs/operators';

import { Colors } from "../../../custom/colors/colors";

@Component({
  selector: 'app-top-debtors',
  templateUrl: './top-debtors.component.html',
  styleUrls: ['./top-debtors.component.scss']
})
export class TopDebtorsComponent implements OnInit {
  private topDebtorsSubject = new BehaviorSubject<User[]>(null);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  topDebtorsCardConstants = new TopDebtorsCardConstants();
  loading = this.loadingSubject.asObservable();

  topDebtors = this.topDebtorsSubject.asObservable();

  error: string;
  message: string;

  balanceColor: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.balanceColor = Colors.DEEP_RED;

    this.loadingSubject.next(true);
    this.refreshData()
    setInterval(() => {
      this.refreshData()
    }, 5000);
  }

  refreshData() {
    this.userService.getTopDebtors().pipe(
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(
      topDebtors => {
        if(topDebtors.users.length != 0) {
          this.topDebtorsCardConstants.title = "Name and Shame"
          this.topDebtorsCardConstants.backgroundColor = Colors.RED_LIGHT;
          this.topDebtorsCardConstants.imageBackgroundColor = Colors.RED_DARK;
          this.balanceColor = Colors.DEEP_RED;
          this.topDebtorsSubject.next(topDebtors.users);
          this.message = '';
          this.error = null;
        } else {
          this.userService.getTopPayers().subscribe(
            topPayers => {
              if(topPayers.users.length != 0) {
                this.topDebtorsCardConstants.title = "Name and Fame"
                this.topDebtorsCardConstants.backgroundColor = Colors.GREEN_LIGHT;
                this.topDebtorsCardConstants.imageBackgroundColor = Colors.GREEN_DARK;
                this.balanceColor = Colors.DEEP_GREEN;
                this.topDebtorsSubject.next(topPayers.users);
                this.message = '';
              } else {
                this.message = "Strangely, there are no users. This might be a problem (Or everyone's balance is exactly R0.0?).";
              }
              this.error = null;
            },
            error => {
              this.topDebtorsSubject.next([]);
              this.error = error;
            }
          );
        }
      },
      error => {
        this.topDebtorsSubject.next([]);
        this.error = error;
      }
    );
  }

}
