import { Colors } from "../../../custom/colors/colors";

export class TopCoffeeDrinkersCardConstants {
  imageUrl = '../../assets/images/trophy.svg';
  title = 'Coffee Champions';
  backgroundColor = Colors.YELLOW_LIGHT;
  imageBackgroundColor = Colors.YELLOW_DARK;
}