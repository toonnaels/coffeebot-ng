import { Component, OnInit } from '@angular/core';
import { TopCoffeeDrinkersCardConstants } from './top-drinkers';
import { BehaviorSubject, of } from 'rxjs';
import { TopDrinkers, TopDrinker } from '../../../models/user-models/user.model';
import { UserService } from '../../../services/user-service/user.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-top-drinkers',
  templateUrl: './top-drinkers.component.html',
  styleUrls: ['./top-drinkers.component.scss']
})
export class TopDrinkersComponent implements OnInit {
  private topDrinkersSubject = new BehaviorSubject<TopDrinker[]>(null);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  topDrinkersCardConstants = new TopCoffeeDrinkersCardConstants();
  loading = this.loadingSubject.asObservable();

  topDrinkersData: TopDrinkers;
  topDrinkers = this.topDrinkersSubject.asObservable();

  timeFrames = ['day', 'week', 'month', 'year'];
  defaultTimeFrame = this.timeFrames[2];
  selectedTime: string;

  error: string;
  message: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.selectedTime = this.defaultTimeFrame;

    this.loadingSubject.next(true);
    this.refreshData();
    setInterval(() => {
      this.refreshData()
    }, 5000);
  }

  refreshData() {
    this.userService.getTopDrinkers().pipe(
      finalize(() => this.loadingSubject.next(false))
    ).subscribe(
      topDrinkers => {
        this.topDrinkersData = topDrinkers;

        if(topDrinkers[this.selectedTime].length != 0) {
          this.topDrinkersSubject.next(topDrinkers[this.selectedTime]);
          this.message = '';
        } else {
          this.topDrinkersSubject.next([]);
          this.updateMessage();
        }

        this.error = null;
      },
      error => {
        this.topDrinkersSubject.next([]);
        this.error = error;
      }
    );
  }

  changeFilter() {
    if(this.topDrinkersData[this.selectedTime].length != 0) {
      this.topDrinkersSubject.next(this.topDrinkersData[this.selectedTime]);
      this.message = '';
    } else {
      this.topDrinkersSubject.next([]);
      this.updateMessage();
    }
  }

  updateMessage() {
    this.message = `Nobody bought coffee ${(this.selectedTime == this.timeFrames[0]) ? "today" : "this " + this.selectedTime}. You poor people...`;
  }

}
