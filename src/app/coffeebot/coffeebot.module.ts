import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoffeebotRoutingModule } from './coffeebot-routing.module';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FooterComponent } from './footer/footer.component';
import { UsersPageComponent } from './user/users-page/users-page.component';
import { CardHeaderComponent } from './display-card/card-header/card-header.component';
import { UserTableComponent } from './user/user-table/user-table.component';
import { CoffeelogTableComponent } from './coffeelog/coffeelog-table/coffeelog-table.component';
import { CoffeelogPageComponent } from './coffeelog/coffeelog-page/coffeelog-page.component';
import { UserCardComponent } from './user/user-card/user-card.component';
import { CoffeelogCardComponent } from './coffeelog/coffeelog-card/coffeelog-card.component';
import { TopDrinkersComponent } from './display-card/top-drinkers/top-drinkers.component';
import { TopDebtorsComponent } from './display-card/top-debtors/top-debtors.component';
import { MaterialModule } from '../material';
import { CoffeebotComponent } from './coffeebot.component';
import { FormsModule } from '@angular/forms';
import { ConcatPipe } from '../custom/pipes/concat.pipe';
import { CustomModule } from '../custom';
import { RecentActivityPageComponent } from './recent-activity/recent-activity-page/recent-activity-page.component';
import { RecentActivityTableComponent } from './recent-activity/recent-activity-table/recent-activity-table.component';
import { RecentActivityCardComponent } from './recent-activity/recent-activity-card/recent-activity-card.component';
import { TopMilkComponent } from './display-card/top-milk/top-milk.component';

@NgModule({
  imports: [
    CommonModule,
    CoffeebotRoutingModule,
    FormsModule,
    MaterialModule,
    CustomModule
  ],
  declarations: [
    CoffeebotComponent,
    NavigationComponent,
    DashboardComponent,
    FooterComponent,
    UsersPageComponent,
    CardHeaderComponent,
    UserTableComponent,
    CoffeelogTableComponent,
    CoffeelogPageComponent,
    UserCardComponent,
    CoffeelogCardComponent,
    TopDrinkersComponent,
    TopMilkComponent,
    TopDebtorsComponent,
    RecentActivityPageComponent,
    RecentActivityTableComponent,
    RecentActivityCardComponent
  ]
})
export class CoffeebotModule { }
